# Order book BTC-PERPETUAL

Local order book copy of BTC-PERPETUAL in Rust

## Getting started

Just `cargo run` it

# Note to reviewer

I have not used `serde` or websocket before, so there is definitely a chance that I implemented something in a suboptimal/wrong way. I tried to focus on (the correctness of) the data structures, but I did not implement extensive checks on the results of the API subscription.  

# Ideas for improvement
- More tests on the data structure
- Verification of valid API subscription output
- Add and check order book invariant: lowest ask >= highest bid
- Add documentation to `lib` components

# Design

## Order book data structure

The chosen data structure of the order book as defined in [orderbook.rs](src/lib/orderbook.rs) is a `struct` containing two `BTreeMap`'s. In the map, the key represents the price, and the value represents the amount. The choice for a `BTreeMap` to model one side of the order book is made because of the efficient O(log(n)) lookup of the smallest and largest elements, as the map is sorted by the keys.

Although it is not specified in the documentation, it is assumed that the prices returned by the API are in half dollar increments. It is also assumed that the amounts are given in natural numbers. Under these assumptions, the type representing the prices in the orderbook is chosen to be `U63F1`, a fixed point integer type with one fractional bit from the [`fixed` package](https://crates.io/crates/fixed). It supports a range from `0` to `9223372036854775807.5` without loss of precision within that range. The current price of a bitcoin is around 42 thousand, so the price must multiply by a factor of 2*10^14 before getting close to this limit. The `f64` type is avoided due to the issues surrounding floating point types, one of which is the lack of `Ord` implementation as required by the `BTreeMap`.

The amount for a certain price is represented with a `u64`. So this orderbook will support numbers up to `18446744073709551615`, which is significantly more than the maximum of 21 million bitcoins that could possibly exist.
