use std::thread;
use std::time::Duration;
use std::{sync::Arc, time::Instant};

use anyhow::{format_err, Result};
use parking_lot::Mutex;
use serde_json::{from_value, Value};
use tungstenite::{connect, Message};

use order_book_btc_perpetual::{action::Action, orderbook::OrderBook, prices::BestPrices};

const API_URL: &str = "wss://www.deribit.com/ws/api/v2";
const CHANNEL: &str = "book.BTC-PERPETUAL.100ms";
const SUBSCRIBE_STRING: &str = r#"{
    "method": "public/subscribe",
    "params": {
        "channels": [
        "book.BTC-PERPETUAL.100ms"
    ]
    },
    "jsonrpc": "2.0",
    "id": 0
}"#;

fn main() -> ! {
    let best_prices: Arc<Mutex<BestPrices>> = Arc::new(Mutex::new(BestPrices::default()));

    let best_prices_print = Arc::clone(&best_prices);
    thread::spawn(move || {
        print_prices(best_prices_print);
    });

    loop {
        if let Result::Err(e) = track_lob(Arc::clone(&best_prices)) {
            best_prices.lock().reset();
            println!("Error: {}", e);
            println!("Reconnecting...");
        }
    }
}

fn track_lob(best_prices: Arc<Mutex<BestPrices>>) -> anyhow::Result<()> {
    let (mut socket, _) = connect(API_URL)?;

    socket.write_message(Message::Text(SUBSCRIBE_STRING.into()))?;

    {
        let msg = socket.read_message()?;
        let v: Value = serde_json::from_str(msg.to_text().unwrap())?;
        if v["result"][0] != CHANNEL {
            format_err!("Error connecting to channel");
        }
    }

    let mut expected_id: Option<u64> = None;
    let mut book = OrderBook::new();

    loop {
        let msg = socket.read_message()?;
        let v: Value = serde_json::from_str(msg.to_text().unwrap())?;
        let change_id: u64 = serde_json::from_value(v["params"]["data"]["change_id"].clone())?;

        if let Some(expected) = expected_id {
            let prev_change_id: u64 =
                serde_json::from_value(v["params"]["data"]["prev_change_id"].clone())?;

            if expected != prev_change_id {
                return Result::Err(format_err!(
                    "`prev_change_id` ({}) not equal to last `change_id` ({})",
                    prev_change_id,
                    expected
                ));
            }
        }
        expected_id = Some(change_id);

        let bids_vec: Vec<Action> = from_value(v["params"]["data"]["bids"].clone())?;
        let asks_vec: Vec<Action> = from_value(v["params"]["data"]["asks"].clone())?;
        book.process_bid_actions(&bids_vec);
        book.process_ask_actions(&asks_vec);

        let lowest_ask = book.get_lowest_ask();
        let highest_bid = book.get_highest_bid();

        best_prices.lock().set(lowest_ask, highest_bid);
    }
}

fn print_prices(best_prices: Arc<Mutex<BestPrices>>) -> ! {
    loop {
        let (best_ask, best_bid, timestamp) = best_prices.lock().get();

        match (best_bid, best_ask) {
            (Some(bid), Some(ask)) => {
                print!(
                    "Bid: ${} ({}), Ask: ${} ({})",
                    bid.0.price, bid.0.amount, ask.0.price, ask.0.amount
                );
            }
            (Some(bid), None) => {
                print!("Bid: ${} ({}), Ask: N/A", bid.0.price, bid.0.amount);
            }
            (None, Some(ask)) => {
                print!("Bid: N/A, Ask ${} ({})", ask.0.price, ask.0.amount);
            }
            (None, None) => {
                print!("Bid: N/A, Ask: N/A");
            }
        }

        if best_bid.is_none() && best_ask.is_none() {
            println!(" (empty)")
        } else if timestamp < Instant::now() - Duration::from_millis(500) {
            println!(" (outdated)");
        } else {
            println!(" (up-to-date)");
        }

        thread::sleep(Duration::from_secs(1));
    }
}
