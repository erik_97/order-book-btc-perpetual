use std::time::Instant;

use fixed::types::U63F1;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct PriceEntry {
    pub price: U63F1,
    pub amount: u64,
}

// Avoid mixing bid and ask prices with newtypes
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct AskPriceEntry(pub PriceEntry);

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct BidPriceEntry(pub PriceEntry);

impl AskPriceEntry {
    pub fn new(price: U63F1, amount: u64) -> Self {
        Self(PriceEntry { price, amount })
    }
}

impl BidPriceEntry {
    pub fn new(price: U63F1, amount: u64) -> Self {
        Self(PriceEntry { price, amount })
    }
}

#[derive(Clone, Copy, Debug)]
pub struct BestPrices {
    pub ask: Option<AskPriceEntry>,
    pub bid: Option<BidPriceEntry>,
    timestamp: Instant,
}

impl BestPrices {
    pub fn new() -> Self {
        Self {
            ask: None,
            bid: None,
            timestamp: Instant::now(),
        }
    }

    pub fn reset(&mut self) {
        self.ask = None;
        self.bid = None;
        self.timestamp = Instant::now();
    }

    pub fn set(&mut self, low_ask: Option<AskPriceEntry>, high_bid: Option<BidPriceEntry>) {
        self.ask = low_ask;
        self.bid = high_bid;
        self.timestamp = Instant::now();
    }

    pub fn get(&self) -> (Option<AskPriceEntry>, Option<BidPriceEntry>, Instant) {
        (self.ask, self.bid, self.timestamp)
    }
}

impl Default for BestPrices {
    fn default() -> Self {
        Self::new()
    }
}
