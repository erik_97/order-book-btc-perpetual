use fixed::types::U63F1;
use serde::{Deserialize, Deserializer};

#[derive(Deserialize, Eq, PartialEq, Debug)]
#[serde(rename_all = "lowercase")]
pub enum ActionType {
    New,
    Change,
    Delete,
}

#[derive(Deserialize)]
struct RawAction {
    actiontype: ActionType,
    price: f64,
    amount: f64,
}

#[derive(Eq, PartialEq, Debug)]
pub struct Action {
    pub actiontype: ActionType,
    pub price: U63F1,
    pub amount: u64,
}

impl<'de> Deserialize<'de> for Action {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let raw = RawAction::deserialize(deserializer)?;

        // TODO: check invariants
        // - price is float ending in .0 or .5
        // - amount is float ending in .0

        Ok(Action {
            actiontype: raw.actiontype,
            price: U63F1::from_num(raw.price),
            amount: raw.amount as u64,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_action_type_new() {
        let expected = ActionType::New;
        let input = r#""new""#;
        let output = serde_json::from_str(input).expect("Unable to deserialize to ActionType");
        assert_eq!(expected, output);
    }

    #[test]
    fn deserialize_action_type_change() {
        let expected = ActionType::Change;
        let input = r#""change""#;
        let output = serde_json::from_str(input).expect("Unable to deserialize to ActionType");
        assert_eq!(expected, output);
    }

    #[test]
    fn deserialize_action_type_delete() {
        let expected = ActionType::Delete;
        let input = r#""delete""#;
        let output = serde_json::from_str(input).expect("Unable to deserialize to ActionType");
        assert_eq!(expected, output);
    }

    #[test]
    fn deserialize_action_new() {
        let expected = Action {
            actiontype: ActionType::New,
            // price: U63F1::from_num(39171),
            price: U63F1::from_num(39171.5),
            amount: 33600,
        };
        let input = r#"[
            "new",
            39171.5,
            33600
          ]"#;
        let output: Action = serde_json::from_str(input).expect("Failed to deserialize to Action");
        assert_eq!(output, expected);
    }

    #[test]
    fn deserialize_action_change() {
        let expected = Action {
            actiontype: ActionType::Change,
            price: U63F1::from_num(45622.0),
            amount: 21800,
        };
        let input = r#"[
            "change",
            45622.0,
            21800
          ]"#;
        let output: Action = serde_json::from_str(input).expect("Failed to deserialize to Action");
        assert_eq!(output, expected);
    }

    #[test]
    fn deserialize_action_delete() {
        let expected = Action {
            actiontype: ActionType::Delete,
            price: U63F1::from_num(45622.0),
            amount: 0,
        };
        let input = r#"[
            "delete",
            45622,
            0
          ]"#;
        let output: Action = serde_json::from_str(input).expect("Failed to deserialize to Action");
        assert_eq!(output, expected);
    }
}
