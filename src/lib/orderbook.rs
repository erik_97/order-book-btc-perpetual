use std::collections::BTreeMap;

use super::action::{Action, ActionType};
use super::prices::{AskPriceEntry, BidPriceEntry};

use fixed::types::U63F1;

pub struct OrderBook {
    bid: BTreeMap<U63F1, u64>,
    ask: BTreeMap<U63F1, u64>,
}

impl OrderBook {
    pub fn new() -> Self {
        Self {
            bid: BTreeMap::new(),
            ask: BTreeMap::new(),
        }
    }

    pub fn get_highest_bid(&self) -> Option<BidPriceEntry> {
        self.bid
            .iter()
            .next_back()
            .map(|(&price, &amount)| BidPriceEntry::new(price, amount))
    }

    pub fn get_lowest_ask(&self) -> Option<AskPriceEntry> {
        self.ask
            .iter()
            .next()
            .map(|(&price, &amount)| AskPriceEntry::new(price, amount))
    }

    pub fn insert_bid(&mut self, key: U63F1, value: u64) {
        self.bid.insert(key, value);
    }

    pub fn insert_ask(&mut self, key: U63F1, value: u64) {
        self.ask.insert(key, value);
    }

    pub fn process_bid_actions(&mut self, changes: &[Action]) {
        for action in changes.iter() {
            match action.actiontype {
                ActionType::New | ActionType::Change => {
                    self.bid.insert(action.price, action.amount);
                }
                ActionType::Delete => {
                    self.bid.remove(&action.price);
                }
            }
        }
    }

    pub fn process_ask_actions(&mut self, changes: &[Action]) {
        for action in changes.iter() {
            match action.actiontype {
                ActionType::New | ActionType::Change => {
                    self.ask.insert(action.price, action.amount);
                }
                ActionType::Delete => {
                    self.ask.remove(&action.price);
                }
            }
        }
    }
}

impl Default for OrderBook {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_lowest_ask_nonempty() {
        let mut nonempty_book = OrderBook::new();
        nonempty_book.insert_ask(U63F1::from_num::<f64>(3.0), 1);
        nonempty_book.insert_ask(U63F1::from_num::<f64>(5.5), 44);
        nonempty_book.insert_ask(U63F1::from_num::<f64>(17.0), 7);

        let price = U63F1::from_num::<f64>(3.0);

        let expected = Some(AskPriceEntry::new(price, 1));
        let output = nonempty_book.get_lowest_ask();
        assert_eq!(expected, output);
    }

    #[test]
    fn get_highest_bid_nonempty() {
        let mut nonempty_book = OrderBook::new();
        nonempty_book.insert_bid(U63F1::from_num::<f64>(3.0), 1);
        nonempty_book.insert_bid(U63F1::from_num::<f64>(5.5), 44);
        nonempty_book.insert_bid(U63F1::from_num::<f64>(17.0), 7);

        let price = U63F1::from_num::<f64>(17.0);

        let expected = Some(BidPriceEntry::new(price, 7));
        let output = nonempty_book.get_highest_bid();
        assert_eq!(expected, output);
    }

    #[test]
    fn bid_actions_new_delete() {
        let mut book = OrderBook::new();
        let actions = vec![
            Action {
                actiontype: ActionType::New,
                price: U63F1::from_num::<f64>(33.5),
                amount: 5,
            },
            Action {
                actiontype: ActionType::Delete,
                price: U63F1::from_num::<f64>(33.5),
                amount: 0,
            },
            Action {
                actiontype: ActionType::New,
                price: U63F1::from_num::<f64>(20.0),
                amount: 11,
            },
        ];

        book.process_bid_actions(&actions);

        let expected = BidPriceEntry::new(U63F1::from_num::<f64>(20.0), 11);
        let result = book.get_highest_bid().unwrap();
        assert_eq!(expected, result);
    }

    #[test]
    fn ask_actions_new_delete() {
        let mut book = OrderBook::new();
        let actions = vec![
            Action {
                actiontype: ActionType::New,
                price: U63F1::from_num::<f64>(11.5),
                amount: 5,
            },
            Action {
                actiontype: ActionType::New,
                price: U63F1::from_num::<f64>(20.5),
                amount: 6,
            },
            Action {
                actiontype: ActionType::Delete,
                price: U63F1::from_num::<f64>(11.5),
                amount: 0,
            },
        ];

        book.process_ask_actions(&actions);

        let expected = AskPriceEntry::new(U63F1::from_num::<f64>(20.5), 6);
        let result = book.get_lowest_ask().unwrap();
        assert_eq!(expected, result);
    }
}
